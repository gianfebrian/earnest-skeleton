from node:4.5.0

ADD package.json /packages/package.json
WORKDIR /packages
RUN npm install

WORKDIR /
RUN mkdir /src && cp -a /packages/node_modules /src
ADD . /src
