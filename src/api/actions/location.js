
import LocationModel from './../models/location';

export default class Location {
  static async fetch(req, res) {
    try {
      const locations = await LocationModel.getAll();

      return res.json(locations);
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  static async get(req, res) {
    try {
      const id = req.params.id;
      const location = await LocationModel.getById(id);

      return res.json(location);
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  static async create(req, res) {
    try {
      const payload = req.body;
      await LocationModel.create(payload);

      return res.status(200).end();
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  static async updateOrder(req, res) {
    try {
      const payloads = req.body;
      await LocationModel.updateOrder(payloads);

      return res.status(200).end();
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  static async update(req, res) {
    try {
      const id = req.params.id;
      const payload = req.body;
      await LocationModel.updateById(id, payload);

      return res.status(200).end();
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  static async destroy(req, res) {
    try {
      const id = req.params.id;
      await LocationModel.destroyById(id);

      return res.status(200).end();
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  static async destroyAll(req, res) {
    try {
      await LocationModel.destroyAll();

      return res.status(200).end();
    } catch (err) {
      return res.status(400).json(err);
    }
  }
}
