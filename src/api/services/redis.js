
import redis from 'redis';
import Promise from 'bluebird';
import dotenv from 'dotenv';

dotenv.load();

const client = redis.createClient(process.env.REDIS_URL);

Promise.promisifyAll(client);

export default client;
