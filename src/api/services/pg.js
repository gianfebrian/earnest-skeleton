
import knex from 'knex';
import dotenv from 'dotenv';

dotenv.load();

const client = knex({
  client: 'postgresql',
  connection: process.env.POSTGRE_URL,
  pool: {
    min: process.env.POSTGRE_MIN_POOL,
    max: process.env.POSTGRE_MAX_POOL,
  },
});

export default client;
