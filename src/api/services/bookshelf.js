
import bookshelf from 'bookshelf';
import pg from './pg';

const Bookshelf = bookshelf(pg);

Bookshelf.plugin('registry');
Bookshelf.plugin('visibility');

export default Bookshelf;
