
import _ from 'lodash';
import mkdirp from 'mkdirp';
import winston from 'winston';
import { Papertrail } from 'winston-papertrail';
import WinstonFile from 'winston-daily-rotate-file';

function createFileTransport() {
  const logDir = process.env.LOG_DIR || 'log';
  mkdirp.sync(logDir);

  createFileTransport.transport = new WinstonFile({
    filename: `${logDir}/filelogger_`,
    datePattern: 'yyy-MM-dd',
    level: process.env.LOG_SEVERITY,
    silent: ((process.env.LOG_SILENT === 'true') === 'true'),
    timestamp: true,
    handleExceptions: true,
    humanReadableUnhandledException: true,
  });

  createFileTransport.transport.getFormattedDate = () => {
    const today = new Date();
    const month = today.getMonth() + 1;
    const date = today.getDate();
    const year = today.getFullYear();
    const logName = `${month}_${date}_${year}.log`;

    return logName;
  };

  return createFileTransport.transport;
}

function createConsoleTransport() {
  const transport = new (winston.transports.Console)({
    colorize: true,
    level: process.env.LOG_SEVERITY,
    silent: (process.env.LOG_SILENT === 'true'),
    timestamp: true,
    handleExceptions: true,
    humanReadableUnhandledException: true,
  });

  return transport;
}

function createPapertrailTransport() {
  const transport = new Papertrail({
    host: process.env.PAPERTRAIL_HOST,
    port: process.env.PAPERTRAIL_PORT,
    level: process.env.LOG_SEVERITY,
    silent: (process.env.LOG_SILENT === 'true'),
    colorize: true,
    handleExceptions: true,
  });

  return transport;
}

function initTransports() {
  const transports = (process.env.LOG_TRANSPORT || 'console').split('|');
  const results = [];
  if (_.includes(transports, 'console')) {
    results.push(createConsoleTransport());
  }
  if (_.includes(transports, 'file')) {
    results.push(createFileTransport());
  }
  if (_.includes(transports, 'papertrail')) {
    results.push(createPapertrailTransport());
  }

  return results;
}

const logger = new winston.Logger({
  transports: initTransports(),
  exitOnError: false,
});

export {
  createFileTransport,
  createConsoleTransport,
  createPapertrailTransport,
  initTransports,
};

export default logger;
