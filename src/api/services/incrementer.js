
import redis from './redis';
import log from './logger';

const BASE_KEY = 'increment';
const ALPHA_MAX = 26;
const ALPHA_MIN = 1;
const ALPHA_INIT = 'A'.charCodeAt(0) - 1;

// only support MAX = 'Z', MIN = 'A'
export default class Incrementer {

  static async increaseAlphabet(key) {
    try {
      const resource = `${BASE_KEY}:${key}`;
      const value = await redis.incrAsync(resource);

      if (value === ALPHA_MAX) {
        await redis.setAsync(resource, ALPHA_MAX);

        const err = new Error('Can not increase. Maximum value.');

        throw err;
      }

      const nextValue = String.fromCharCode(ALPHA_INIT + value);

      return nextValue;
    } catch (err) {
      log.error(err);

      throw err;
    }
  }

  static async decreaseAlphabet(key) {
    try {
      const resource = `${BASE_KEY}:${key}`;
      const value = await redis.decrAsync(resource);

      if (value < ALPHA_MIN) {
        await redis.setAsync(resource, ALPHA_MIN);

        const err = new Error('Can not decrease. Minimum value.');

        throw err;
      }

      const nextValue = String.fromCharCode(ALPHA_INIT + value);

      return nextValue;
    } catch (err) {
      log.error(err);

      throw err;
    }
  }

  // Get current value without locking (possibly, not the actual current value)
  static async getCurrentIncrementAlphabet(key) {
    try {
      const resource = `${BASE_KEY}:${key}`;
      const value = Number(await redis.getAsync(resource));

      if (value < 1) {
        const err = new Error('Value not set.');

        throw err;
      }

      const currentValue = String.fromCharCode(ALPHA_INIT + value);

      return currentValue;
    } catch (err) {
      log.error(err);

      throw err;
    }
  }

  static async reset(key) {
    const resource = `${BASE_KEY}:${key}`;
    await redis.setAsync(resource, 0);

    const value = Number(await redis.getAsync(resource));

    return value;
  }
}
