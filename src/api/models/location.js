/* eslint-disable class-methods-use-this */

import _ from 'lodash';
import uuid from 'uuid';
import Bookshelf from './../services/bookshelf';
import Incrementer from './../services/incrementer';

class Location extends Bookshelf.Model {
  get tableName() {
    return 'locations';
  }

  get hasTimestamps() {
    return true;
  }

  initialize() {
    this.on('creating', this.onCreating, this);

    this.on('destroying', this.onDestroying, this);
  }

  async onCreating(model) {
    const label = await Incrementer.increaseAlphabet(this.tableName);
    model.set('label', label);
  }

  async onDestroying(model) {
    try {
      if (!_.isEmpty(model.get('label'))) {
        return null;
      }

      const location = await model.fetch();
      const currentIncrement = await Incrementer.getCurrentIncrementAlphabet(this.tableName);

      if (location.get('label') === currentIncrement) {
        await Incrementer.decreaseAlphabet(this.tableName);
      }

      return location;
    } catch (err) {
      return null;
    }
  }

  static async getAll() {
    const model = new this();
    const result = await model.orderBy('label', 'ASC').fetchAll();

    return result;
  }

  static async getById(id) {
    const model = new this();
    const result = await model.where({ id }).fetch();

    return result;
  }

  static async create(item) {
    const record = item;
    record.id = uuid.v4();

    const model = new this();
    const result = await model.save(item, { method: 'insert' });

    return result;
  }

  static async updateOrder(collection) {
    const result = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const item of collection) {
      const model = new this({ id: item.id });
      result.push(await model.save(item, { method: 'update' }));
    }

    return result;
  }

  static async updateById(id, item) {
    const model = new this({ id });
    const result = await model.save(item, { method: 'update' });

    return result;
  }

  static async destroyById(id) {
    const model = new this({ id });
    const result = await model.destroy();

    return result;
  }

  // individually destroy to trigger destroying event
  static async destroyAll() {
    const result = [];

    const collection = await this.forge().orderBy('label', 'DESC').fetchAll();
    // eslint-disable-next-line no-restricted-syntax
    for (const item of collection.models) {
      const model = new this({
        id: item.id,
        label: item.get('label'),
      });
      result.push(await model.destroy());
    }

    await Incrementer.reset(this.forge().tableName);

    return result;
  }
}

export default Bookshelf.model('Location', Location);
