
import Location from './actions/location';

function routes(app) {
  app.get('/locations', Location.fetch);
  app.get('/location/:id', Location.get);
  app.post('/location', Location.create);
  app.put('/location', Location.updateOrder);
  app.patch('/location/:id', Location.update);
  app.delete('/location/:id', Location.destroy);
  app.delete('/locations', Location.destroyAll);
}

export default routes;
