
import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
// eslint-disable-next-line import/no-extraneous-dependencies
import cors from 'cors';
import routes from './routes';
import log from './services/logger';

const PORT = process.env.PORT || 8080;
const MORGAN_FORMAT = process.env.MORGAN_FORMAT || 'dev';
const app = express();

app.use(express.static(`${__dirname}/../../public`));
app.use(morgan(MORGAN_FORMAT, { stream: { write(message) { log.info(message); } } }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

if (process.env.NODE_ENV === 'development') {
  app.use(cors());
}

routes(app);

async function start() {
  log.info('Starting server');
  await app.listen(PORT);
  log.info(`Server is running on port ${PORT}`);
}

async function stop() {
  log.info('Stopping server');
  await app.close();
}

export default { app, start, stop };
