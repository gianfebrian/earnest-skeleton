
import 'reflect-metadata';
import 'zone.js';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';
import { enableProdMode, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertModule, ModalModule, ButtonsModule } from 'ng2-bootstrap';

import { AgmCoreModule } from 'angular2-google-maps/core';
import { SortablejsModule } from 'angular-sortablejs';

import AppComponent from './components/app/component';
import FormComponent from './components/form/component';
import MapDirectionDirective from './components/map/direction.directive';
import MapGeocoderDirective from './components/map/geocoder.directive';
import LocationAPIService from './services/location-api';

if (process.env.NODE_ENV === 'production') {
  enableProdMode();
}

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    MapGeocoderDirective,
    MapDirectionDirective,
  ],
  providers: [LocationAPIService],
  imports: [
    HttpModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    ButtonsModule.forRoot(),
    SortablejsModule,
    AgmCoreModule.forRoot({
      apiKey: process.env.GOOGLE_API_KEY,
    }),
  ],
  bootstrap: [AppComponent],
})
class AppModule {}

platformBrowserDynamic().bootstrapModule(AppModule);

