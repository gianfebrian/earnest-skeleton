
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

const API_PROTOCOL = process.env.API_PROTOCOL;
const API_HOST = process.env.API_HOST;
const API_PORT = process.env.API_PORT;

@Injectable()
export default class LocationAPIService {
  remoteLocations = new BehaviorSubject([]);

  constructor(http: Http) {
    this.http = http;
    this.API_BASE_URI = `${API_PROTOCOL}://${API_HOST}${(API_PORT === '' ? '' : `:${API_PORT}`)}`;

    const headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers });
  }

  refresh() {
    const locationsResponse = this.http.get(`${this.API_BASE_URI}/locations`)
      .map(res => res.json());

    locationsResponse.subscribe(
      (locations) => {
        this.remoteLocations.next(locations);
      },
    );

    return locationsResponse;
  }

  add(location) {
    return this.http.post(`${this.API_BASE_URI}/location`, JSON.stringify(location), this.options);
  }

  get(id) {
    return this.http.get(`${this.API_BASE_URI}/location/${id}`)
      .map(res => res.json());
  }

  update(location) {
    return this.http.post(`${this.API_BASE_URI}/location/${location.id}`, JSON.stringify(location))
      .map(res => res.json());
  }

  destroyById(id) {
    return this.http.delete(`${this.API_BASE_URI}/location/${id}`);
  }

  destroyAll(id) {
    return this.http.delete(`${this.API_BASE_URI}/locations`);
  }

  updateOrder(locationCollection) {
    return this.http.put(
      `${this.API_BASE_URI}/location`,
      JSON.stringify(locationCollection),
      this.options // eslint-disable-line comma-dangle
    );
  }
}
