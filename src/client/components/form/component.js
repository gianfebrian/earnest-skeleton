/* eslint-disable no-underscore-dangle, dot-notation */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import template from './template.html';

@Component({
  selector: 'es-form',
  template,
})
export default class FormComponent {
  @Input() data;

  @Output() saved = new EventEmitter();

  constructor(builder: FormBuilder) {
    this._builder = builder;

    this.form = this._builder.group({
      id: [''],
      title: [''],
      description: [''],
      label: [''],
      longitude: [''],
      latitude: [''],
    });
  }

  ngOnChanges(change) {
    if (change.data && change.data.currentValue) {
      this.form.controls['id'].setValue(change.data.currentValue.id);
      this.form.controls['title'].setValue(change.data.currentValue.title);
      this.form.controls['description'].setValue(change.data.currentValue.description);
      this.form.controls['label'].setValue(change.data.currentValue.label);
      this.form.controls['longitude'].setValue(change.data.currentValue.longitude);
      this.form.controls['latitude'].setValue(change.data.currentValue.latitude);
    }
  }

  onSubmit(data) {
    this.saved.emit(data);
  }
}
