
import _ from 'lodash';
import { Component, ViewChild, NgZone } from '@angular/core';

import template from './template.html';
import LocationAPIService from './../../services/location-api';

@Component({
  selector: 'es-app',
  template,
  styles: [`
    :host >>> .alert {
      margin-bottom: 5px;
    }
    #right-panel, .sebm-google-map-container {
      height: 400px;
    }
    .panel-heading h3 {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      line-height: normal;
      padding-top: 8px;
    }
    .panel-body {
      padding: 5px;
      height: 340px;
      overflow-y: scroll;
    }
  `,
  ],
})
export default class AppComponent {
  @ViewChild('modal') modal;

  directionModel = '';
  directionOptions = {};
  directionRenderer;
  mapZoom = 15;
  mapCenter = {};

  constructor(locationAPIService: LocationAPIService, zone: NgZone) {
    this.zone = zone;
    this.locationAPIService = locationAPIService;
  }

  routeModeChange(change) {
    if (change === 'removeAll') {
      this.distanceCalculation = null;
      this.directionRenderer = false;
      return this.locationAPIService.destroyAll().subscribe(
        () => {
          this.locationAPIService.refresh();
        },
      );
    }

    if (change === 'clear') {
      this.distanceCalculation = null;
      this.directionRenderer = false;
      return true;
    }

    this.directionOptions = {
      travelMode: change,
    };

    this.directionRenderer = true;

    return true;
  }

  ngOnInit() {
    this.locationAPIService.refresh();
    this.getRemoteLocations().subscribe(
      (locations) => {
        if (_.isEmpty(locations)) {
          this.mapCenter = { lat: -6.218658, lng: 106.802645 };

          return this.mapCenter;
        }

        const centerLocation = _.first(locations);

        this.mapCenter = {
          lat: centerLocation.latitude,
          lng: centerLocation.longitude,
        };

        return this.mapCenter;
      },
    );
    this.sortableOptions = {
      scrollSpeed: 50,
      onUpdate: this.onSortableUpdate.bind(this),
    };
  }

  getRemoteLocations() {
    return this.locationAPIService.remoteLocations;
  }

  onMapClick(e) {
    this.zone.run(() => {
      this.data = {
        title: e.geocode.formatted_address,
        latitude: e.coords.lat,
        longitude: e.coords.lng,
      };

      this.modal.show();
    });
  }

  onAfterDirectionRender(e) {
    this.distanceCalculation = e;
  }

  onAlertClosed(e) {
    this.locationAPIService.destroyById(e).subscribe(
      () => {
        this.locationAPIService.refresh();
      },
    );
  }

  onAlertClick(e) {
    this.mapCenter = { lat: e.latitude, lng: e.longitude };
  }

  onModalCloseClick() {
    this.modal.hide();
  }

  onSortableUpdate() {
    const currentOrder = this.getRemoteLocations().getValue();
    const changes = _.transform(currentOrder, (t, v, k) => {
      const transformed = v;
      transformed.label = String.fromCharCode('A'.charCodeAt(0) + k);
      t.push(transformed);
    }, []);

    this.locationAPIService.updateOrder(changes).subscribe(
      () => {
        this.locationAPIService.refresh();
      },
    );
  }

  onSave(data) {
    this.locationAPIService.add(data).subscribe(
      () => {
        this.modal.hide();
        this.locationAPIService.refresh();
      },
    );
  }
}
