/* global google */

import _ from 'lodash';
import humanizeDuration from 'humanize-duration';
import { Directive, Input, Output, EventEmitter } from '@angular/core';
import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';

@Directive({
  selector: 'es-map-direction',
})
export default class MapDirection {
  @Input() options = {};
  @Input() render = false;
  @Input() data = [];

  @Output() onAfterRendered = new EventEmitter();

  constructor(apiWrapper: GoogleMapsAPIWrapper) {
    this.apiWrapper = apiWrapper;
  }

  async ngOnInit() {
    this.map = await this.apiWrapper.getNativeMap();
    this.direction = new google.maps.DirectionsService();
    this.directionRenderer = new google.maps.DirectionsRenderer({
      suppressMarkers: true,
    });
    this.distanceMatrix = new google.maps.DistanceMatrixService();
  }

  ngOnChanges(changes) {
    if ((changes.render && changes.render.currentValue === true) ||
      (changes.options && changes.options.currentValue.travelMode)) {
      return this.showDirection();
    }

    if (changes.render && changes.render.currentValue === false) {
      return this.directionRenderer.setMap(null);
    }

    return null;
  }

  getRoutes() {
    let origin = this.options.origin || {};
    if (!_.isEmpty(this.data)) {
      const first = _.first(this.data);
      origin = { lat: first.latitude, lng: first.longitude };
    }

    let destination = this.options.destination || {};
    if (!_.isEmpty(this.data) && (this.data.length > 1)) {
      const last = _.last(this.data);
      destination = { lat: last.latitude, lng: last.longitude };
    }

    let waypoints = this.options.waypoints || [];
    if (!_.isEmpty(this.data)) {
      waypoints = _.transform(this.data, (t, v, k) => {
        if (k === 0) return;
        if (k === (this.data.length - 1)) return;
        t.push({
          location: {
            lat: v.latitude,
            lng: v.longitude,
          },
          stopover: true,
        });
      }, []);
    }

    if ((!_.isEmpty(origin)) && (!_.isEmpty(destination))) {
      return { origin, destination, waypoints };
    }

    throw new Error('Incomplete route data');
  }

  async showDirection() {
    try {
      const routes = this.getRoutes();

      this.directionRenderer.setMap(this.map);
      this.direction.route({
        origin: routes.origin,
        destination: routes.destination,
        waypoints: routes.waypoints,
        optimizeWaypoints: true,
        unitSystem: this.options.unitSystem || google.maps.UnitSystem.METRIC,
        travelMode: google.maps.TravelMode[this.options.travelMode] ||
          google.maps.TravelMode.DRIVING,
        avoidHighways: this.options.avoidHighways || false,
        avoidTolls: this.options.avoidTolls || false,
      }, (response, status) => {
        if (status !== 'OK') return console.log(status); //eslint-disable-line

        this.directionRenderer.setDirections(response);

        const calculated = this.calculateDistanceDuration(response);
        return this.onAfterRendered.emit(calculated);
      });
    } catch (err) {
      console.log(err); // eslint-disable-line
    }
  }

  // eslint-disable-next-line class-methods-use-this
  calculateDistanceDuration(geocodeWaypoints) {
    const route = _.first(geocodeWaypoints.routes);
    const result = {
      distance: {
        value: 0,
        text: '',
      },
      duration: {
        value: 0,
        text: '',
      },
    };

    // eslint-disable-next-line no-restricted-syntax
    for (const leg of route.legs) {
      result.distance.value += leg.distance.value;
      result.duration.value += leg.duration.value;
    }

    result.distance.text = (result.distance.value < 1000) ? `${result.distance.value} m` :
      `${(result.distance.value / 1000).toFixed(1)} km`;

    const shortDuration = humanizeDuration.humanizer({
      language: 'shortEn',
      languages: {
        shortEn: {
          y() { return 'y'; },
          mo() { return 'mo'; },
          w() { return 'wk'; },
          d() { return 'd'; },
          h() { return 'hrs'; },
          m() { return 'mins'; },
        },
      },
    });
    result.duration.text = shortDuration(
      result.duration.value * 1000,
      {
        units: ['d', 'h', 'm'],
        round: true,
      },
    );

    return result;
  }
}

