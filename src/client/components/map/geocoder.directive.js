/* global google */

import _ from 'lodash';
import { Directive, EventEmitter, Output, Input } from '@angular/core';
import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';

@Directive({
  selector: 'es-map-geocoder',
})
export default class MapGeoCoder {
  @Input() center;
  @Input() zoom;

  @Output() mapClick = new EventEmitter();

  constructor(apiWrapper: GoogleMapsAPIWrapper) {
    this.apiWrapper = apiWrapper;
  }

  async ngOnChanges(change) {
    if (change.center) {
      await this.apiWrapper.panTo(change.center.currentValue);
    }

    if (change.zoom) {
      await this.apiWrapper.setZoom(change.zoom.currentValue);
    }
  }

  ngOnInit() {
    this.apiWrapper.subscribeToMapEvent('click').subscribe(this.onClick.bind(this));
  }

  onClick(e) {
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ location: e.latLng }, (results, status) => {
      if (status !== 'OK') return console.log(status); // eslint-disable-line

      return this.mapClick.emit({
        coords: {
          lat: e.latLng.lat(),
          lng: e.latLng.lng(),
        },
        geocode: _.first(results),
      });
    });
  }
}

