require('dotenv').load();

module.exports = {
  client: 'postgresql',
  connection: process.env.POSTGRE_URL,
  pool: {
    min: process.env.POSTGRE_MIN_POOL,
    max: process.env.POSTGRE_MAX_POOL,
  },
  migrations: {
    tableName: 'knex_migrations',
    directory: './database/migrations',
  },
  seeds: {
    directory: './database/seeders',
  },
};
