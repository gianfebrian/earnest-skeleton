
'use strict'; // eslint-disable-line

/**
 * Require redis and incrementer to sync incr value
 */

const uuid = require('uuid');

const faker = require('faker');

const Incrementer = require('./../../build/api/services/incrementer').default;

exports.seed = function seed(knex, Promise) {
  // Deletes ALL existing entries
  return knex('locations').del()
    .then(function resetingIncr() {
      return Incrementer.reset('locations');
    })
    .then(function seeding() {
      const seeders = [];
      const char = 'A'.charCodeAt(0);
      // eslint-disable-next-line no-plusplus
      for (let i = 0; i < 10; i++) {
        seeders.push(
          knex('locations').insert({
            id: uuid.v4(),
            title: faker.lorem.words(),
            description: faker.lorem.words(),
            label: String.fromCharCode(char + i),
            latitude: Number(faker.address.longitude()),
            longitude: Number(faker.address.latitude()),
            created_at: new Date(),
            updated_at: new Date(),
            deleted_at: new Date(),
          }),
          Incrementer.increaseAlphabet('locations') // eslint-disable-line
        );
      }

      return Promise.all(seeders);
    });
};

