
exports.up = function up(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('locations', function createTableIfNotExists(table) {
      table.uuid('id').primary();
      table.string('title');
      table.string('description');
      table.string('label');
      table.float('latitude');
      table.float('longitude');
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at');
      table.timestamp('deleted_at');
    }),
  ]);
};

exports.down = function down(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('locations'),
  ]);
};
