[![build status](https://gitlab.com/gianfebrian/earnest-skeleton/badges/master/build.svg)](https://gitlab.com/gianfebrian/earnest-skeleton/commits/master)
[![coverage report](https://gitlab.com/gianfebrian/earnest-skeleton/badges/master/coverage.svg)](https://gitlab.com/gianfebrian/earnest-skeleton/commits/master)

### Earnest Skeleton ###
Ernest Shackleton the Antarctica explorer. Unlike his adventurous journey, full with obstacles, earnest skeleton is only a simple google map API exploration using Angular 2.

[Preview Live Demo](https://earnest-skeleton.herokuapp.com)

#### Running this project: ####
**Please rename .env.default into .env and customize it based on your environment.**

Not using docker:
`````bash
# Install node dependencies
npm install

# Database migration
npm run migrate
# Run api server in development mode (--watch)
npm run api:start:dev
# Run client in development mode (--watch)
npm run client:start:dev

# Run api linter
npm run api:lint
# Run client linter
npm run client:lint

# Build api
npm run api:build
# Build client
npm run client:build

# Unit test
npm run api:test:unit
# Integration test
npm run api:test:integration
`````

Using docker and docker-compose:
`````bash
# build
docker-compose build web
docker-compose build integration-test-api
docker-compose build unit-test-api

# Run tests
docker-compose up unit-test
docker-compose up integration-test-api

# Run app
docker-compose up -d web
`````
