/* eslint-disable import/no-extraneous-dependencies, strict */

'use strict';

require('dotenv').load();

const webpack = require('webpack');

const webpackHotMiddleware = require('webpack-hot-middleware');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const CleanPlugin = require('clean-webpack-plugin');

const DotenvPlugin = require('dotenv-webpack');

const ENV = process.env.NODE_ENV || 'development';
const CLIENT_PORT = process.env.CLIENT_PORT || 8081;

const config = {
  entry: './src/client/bootstrap.js',
  output: {
    path: `${__dirname}/public`,
    filename: 'build.js',
  },

  module: {
    loaders: [
      // load and compile javascript
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'angular2'],
        },
      },

      // load css and process less
      { test: /\.css$/, loader: 'style!css' },

      // load JSON files and HTML
      { test: /\.json$/, loader: 'json' },
      { test: /\.html$/, exclude: /(node_modules)/, loader: 'raw' },

      // load fonts(inline base64 URLs for <=8k)
      { test: /\.(ttf|eot|svg|otf)$/, loader: 'file' },
      { test: /\.woff(2)?$/, loader: 'url?limit=8192&minetype=application/font-woff' },

      // load images (inline base64 URLs for <=8k images)
      { test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192' },
    ],
    noParse: [/.+zone\.js\/dist\/.+/, /.+angular2\/bundles\/.+/],
  },

  // inject js bundle to index.html
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/client/index.html',
      inject: 'body',
      minify: false,
    }),
    new DotenvPlugin({
      path: './.env',
      safe: false,
    }),
  ],

  // webpack dev server configuration
  devServer: {
    contentBase: './src/client',
    port: CLIENT_PORT,
    noInfo: false,
    historyApiFallback: false,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000,
    },
    setup: function setup(app) {
      const compiler = webpack(config);
      app.use(webpackHotMiddleware(compiler));
    },
  },
};

if (ENV === 'development') {
  config.devtool = 'cheap-source-map';
  config.plugins = config.plugins.concat([
    new webpack.HotModuleReplacementPlugin(),
  ]);
}

if (ENV === 'production') {
  config.plugins.push(new CleanPlugin(['dist']));
}

module.exports = config;
