/* global describe */
/* eslint-disable global-require */

require('dotenv').load();
require('./shared');

describe('Unit Test', function testCase() {
  require('./unit/logger.js');
  require('./unit/incrementer.js');
  require('./unit/location.js');
});
