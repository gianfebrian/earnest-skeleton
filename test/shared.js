/* global describe */
/* eslint-disable global-require */

const chai = require('chai');
const factories = require('chai-factories');

global.chai = chai;
global.assert = chai.assert;

chai.use(factories);

// register factories
require('./factories/location');

// supress all error
process.env.LOG_SILENT = 'true';
