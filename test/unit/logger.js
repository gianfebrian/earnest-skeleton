/* global describe, before, after, beforeEach, afterEach, it, chai, assert */

'use strict'; // eslint-disable-line

const logger = require('./../../build/api/services/logger');

const DEFAULT_TRANSPORT = process.env.LOG_TRANSPORT;
const DEFAULT_LOG_DIR = process.env.LOG_DIR;
const DEFAULT_PAPERTRAIL_HOST = process.env.PAPERTRAIL_HOST;
const DEFAULT_PAPERTRAIL_PORT = process.env.PAPERTRAIL_PORT;

describe('logger', function testCase() {
  afterEach(function setup() {
    process.env.LOG_TRANSPORT = DEFAULT_TRANSPORT;
    process.env.LOG_DIR = DEFAULT_LOG_DIR;
    process.env.PAPERTRAIL_HOST = DEFAULT_PAPERTRAIL_HOST;
    process.env.PAPERTRAIL_PORT = DEFAULT_PAPERTRAIL_PORT;
  });

  it('logger', function assertion() {
    const log = logger.default;
    assert.isFunction(log.info);
    assert.isFunction(log.warn);
    assert.isFunction(log.error);
    assert.isFunction(log.verbose);
    assert.isFunction(log.debug);
    assert.isFunction(log.silly);
  });

  it('.createFileTransport()', function assertion() {
    const createFileTransport = logger.createFileTransport;
    assert.isFunction(createFileTransport);

    process.env.LOG_TRANSPORT = 'file';
    process.env.LOG_DIR = 'logs';

    const transport = createFileTransport();
    assert.isObject(transport);

    process.env.LOG_DIR = '';
    const transportWithDefaultLogDir = createFileTransport();
    assert.isObject(transportWithDefaultLogDir);

    const logName = transport.getFormattedDate();
    const today = new Date();
    assert.equal(logName, `${today.getMonth() + 1}_${today.getDate()}_${today.getFullYear()}.log`);
  });

  it('.createConsoleTransport()', function assertion() {
    const createConsoleTransport = logger.createConsoleTransport;
    assert.isFunction(createConsoleTransport);

    process.env.LOG_TRANSPORT = 'console';

    const transport = createConsoleTransport();
    assert.isObject(transport);
  });

  it('.createPapertrailTransport()', function assertion() {
    const createPapertrailTransport = logger.createPapertrailTransport;
    assert.isFunction(createPapertrailTransport);

    process.env.LOG_TRANSPORT = 'papertrail';
    // test host provided by papertrail logs.papertrailapp.com:514
    process.env.PAPERTRAIL_HOST = 'logs.papertrailapp.com';
    process.env.PAPERTRAIL_PORT = 514;

    const transport = createPapertrailTransport();
    assert.isObject(transport);
  });

  it('.initTransport()', function assertion() {
    const initTransports = logger.initTransports;
    assert.isFunction(initTransports);

    process.env.LOG_TRANSPORT = '';
    const transportDefault = initTransports();
    assert.isArray(transportDefault);

    process.env.LOG_TRANSPORT = 'console';
    const transportConsole = initTransports();
    assert.isArray(transportConsole);

    process.env.LOG_TRANSPORT = 'file';
    process.env.LOG_DIR = 'logs';
    const transportFile = initTransports();
    assert.isArray(transportFile);

    process.env.LOG_TRANSPORT = 'papertrail';
    // test host provided by papertrail logs.papertrailapp.com:514
    process.env.PAPERTRAIL_HOST = 'logs.papertrailapp.com';
    process.env.PAPERTRAIL_PORT = 514;
    const transportPapertrail = initTransports();
    assert.isArray(transportPapertrail);
  });
});
