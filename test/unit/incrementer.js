/* global describe, before, after, beforeEach, afterEach, it, chai, assert */

'use strict'; // eslint-disable-line

const mockery = require('mockery');

const async = require('async');

describe('Incrementer', function testCase() {
  const key = 'test';

  // eslint-disable-next-line no-unused-vars
  let Incrementer;

  before(function setup(done) {
    mockery.enable({ warnOnReplace: false, warnOnUnregistered: false });
    mockery.registerAllowable('./../../build/api/services/incrementer', true);
    mockery.registerSubstitute('redis', 'redis-js');

    // eslint-disable-next-line global-require
    Incrementer = require('./../../build/api/services/incrementer').default;

    done();
  });

  after(function setup(done) {
    Incrementer.reset(key)
      .then(function reset() {
        mockery.deregisterSubstitute('redis');
        mockery.disable();
        done();
      })
      .catch(done);
  });

  beforeEach(function setup(done) {
    Incrementer.reset(key)
      .then(done)
      .catch(done);
  });

  it('Incrementer()', function assertion() {
    assert.isFunction(Incrementer);
    assert.isFunction(Incrementer.increaseAlphabet);
    assert.isFunction(Incrementer.decreaseAlphabet);
    assert.isFunction(Incrementer.getCurrentIncrementAlphabet);
    assert.isFunction(Incrementer.reset);
  });

  it('.increaseAlphabet()', function assertion(done) {
    async.waterfall([
      function first(callback) {
        Incrementer.increaseAlphabet(key)
          .then(function increased(value) {
            assert.equal(value, 'A');
            callback();
          })
          .catch(callback);
      },
      function second(callback) {
        Incrementer.increaseAlphabet(key)
          .then(function increased(value) {
            assert.equal(value, 'B');
            callback();
          })
          .catch(callback);
      },
    ], done);
  });

  it('.increaseAlphabet() - throw exception', function assertion(done) {
    async.timesSeries(26, function iteratee(n, next) {
      Incrementer.increaseAlphabet(key)
        .then(function increased() {
          next();
        })
        .catch(next);
    }, function result(err) {
      if (!err) {
        return done(new Error('Exception not thrown'));
      }

      assert.equal(err.message, 'Can not increase. Maximum value.');
      return done();
    });
  });

  it('.decreaseAlphabet()', function assertion(done) {
    async.waterfall([
      function initialization(callback) {
        Promise.all([
          Incrementer.increaseAlphabet(key),
          Incrementer.increaseAlphabet(key),
        ])
          .then(function results() {
            callback();
          })
          .catch(callback);
      },
      function decreasing(callback) {
        Incrementer.decreaseAlphabet(key)
          .then(function decreased(value) {
            assert.equal(value, 'A');
            callback();
          })
          .catch(callback);
      },
    ], done);
  });

  it('.decreaseAlphabet() - throw exception', function assertion(done) {
    Incrementer.decreaseAlphabet(key)
      .then(function decreased() {
        done(new Error('Exception not thrown'));
      })
      .catch(function exception(err) {
        assert.equal(err.message, 'Can not decrease. Minimum value.');
        done();
      });
  });

  it('.getCurrentIncrementAlphabet()', function assertion(done) {
    async.waterfall([
      function initialization(callback) {
        Promise.all([
          Incrementer.increaseAlphabet(key),
          Incrementer.increaseAlphabet(key),
        ])
          .then(function results() {
            callback();
          })
          .catch(callback);
      },
      function gettingCurrentValue(callback) {
        Incrementer.getCurrentIncrementAlphabet(key)
          .then(function current(value) {
            assert.equal(value, 'B');
            callback();
          })
          .catch(callback);
      },
    ], done);
  });

  it('.getCurrentIncrementAlphabet() - throw exception', function assertion(done) {
    Incrementer.getCurrentIncrementAlphabet(key)
      .then(function current() {
        done(new Error('Exception not thrown'));
      })
      .catch(function exception(err) {
        assert.equal(err.message, 'Value not set.');
        done();
      });
  });

  it('.reset()', function assertion(done) {
    async.waterfall([
      function initialization(callback) {
        Promise.all([
          Incrementer.increaseAlphabet(key),
          Incrementer.increaseAlphabet(key),
        ])
          .then(function results() {
            callback();
          })
          .catch(callback);
      },
      function resetting(callback) {
        Incrementer.reset(key)
          .then(function current(value) {
            assert.equal(value, 0);
            callback();
          })
          .catch(callback);
      },
    ], done);
  });
});
