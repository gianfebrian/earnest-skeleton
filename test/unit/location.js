/* global describe, before, after, beforeEach, afterEach, it, chai, assert */

'use strict'; // eslint-disable-line

const async = require('async');

const mockKnex = require('mock-knex');

const mockery = require('mockery');

const pg = require('./../../build/api/services/pg').default;

const tracker = mockKnex.getTracker();

describe('Location', function testCase() {
  let LocationModel;
  let locationTableName;
  let Incrementer;

  before(function setup() {
    mockKnex.mock(pg);

    mockery.enable({ warnOnReplace: false, warnOnUnregistered: false });
    mockery.registerAllowable('./../../build/api/models/location', true);
    mockery.registerSubstitute('redis', 'redis-js');

    // eslint-disable-next-line global-require
    LocationModel = require('./../../build/api/models/location').default;
    locationTableName = LocationModel.forge().tableName;
    // eslint-disable-next-line global-require
    Incrementer = require('./../../build/api/services/incrementer').default;
  });

  after(function setup() {
    mockery.deregisterAll();
    mockery.disable();
  });

  beforeEach(function setup() {
    tracker.install();
  });

  afterEach(function setup() {
    Incrementer.reset(locationTableName);
    tracker.uninstall();
  });

  it('LocationModel()', function assertion() {
    assert.isFunction(LocationModel);
    assert.isFunction(LocationModel.getById);
    assert.isFunction(LocationModel.create);
    assert.isFunction(LocationModel.updateOrder);
    assert.isFunction(LocationModel.updateById);
    assert.isFunction(LocationModel.destroyById);
    assert.isFunction(LocationModel.destroyAll);
  });

  it('.getAll()', function assertion(done) {
    const location = chai.create('location');

    tracker.on('query', function mockResult(query, step) {
      [
        function firstQuery() {
          query.response([location]);
        },
      ][step - 1]();
    });

    LocationModel.getAll('test')
      .then(function fetched(data) {
        assert.isArray(data.serialize());
        done();
      })
      .catch(done);
  });

  it('.getById()', function assertion(done) {
    const location = chai.create('location');

    tracker.on('query', function mockResult(query, step) {
      [
        function firstQuery() {
          query.response([location]);
        },
      ][step - 1]();
    });

    LocationModel.getById('test')
      .then(function fetched(data) {
        assert.isObject(data);
        done();
      })
      .catch(done);
  });

  it('.create()', function assertion(done) {
    const location = chai.create('location');
    delete location.id;

    tracker.on('query', function mockResult(query, step) {
      [
        function firstQuery() {
          query.response([location]);
        },
      ][step - 1]();
    });

    LocationModel.create(location)
      .then(function created(data) {
        assert.isObject(data);
        assert.isString(data.id);
        assert.equal(data.get('label'), 'A');
        done();
      })
      .catch(done);
  });

  it('.updateOrder()', function assertion(done) {
    const locations = [chai.create('location'), chai.create('location')];

    tracker.on('query', function mockResult(query, step) {
      [
        function firstQuery() {
          query.response([locations[0]]);
        },
        function secondQuery() {
          query.response([locations[0]]);
        },
      ][step - 1]();
    });

    LocationModel.updateOrder(locations)
      .then(function created(data) {
        assert.isArray(data);
        done();
      })
      .catch(done);
  });

  it('.updateById()', function assertion(done) {
    const location = chai.create('location');

    tracker.on('query', function mockResult(query, step) {
      [
        function firstQuery() {
          query.response([location]);
        },
      ][step - 1]();
    });

    LocationModel.updateById(location.id, location)
      .then(function created(data) {
        assert.isObject(data);
        done();
      })
      .catch(done);
  });

  it('.destroyById()', function assertion(done) {
    const location = chai.create('location', { label: 'B' });

    async.timesSeries(2, function iteratee(n, next) {
      Incrementer.increaseAlphabet(locationTableName)
        .then(function increased() {
          next();
        })
        .catch(next);
    }, function result(err) {
      if (err) done(err);
    });

    tracker.on('query', function mockResult(query, step) {
      [
        function firstQuery() {
          query.response([location]);
        },
        function secondQuery() {
          query.response([{}]);
        },
      ][step - 1]();
    });

    LocationModel.destroyById(location.id)
      .then(function created(data) {
        assert.isObject(data);
        done();
      })
      .catch(done);
  });

  it('.destroyById() - current minimum label', function assertion(done) {
    const location = chai.create('location', { label: 'A' });

    tracker.on('query', function mockResult(query, step) {
      [
        function firstQuery() {
          query.response([location]);
        },
        function secondQuery() {
          query.response([{}]);
        },
      ][step - 1]();
    });

    LocationModel.destroyById(location.id)
      .then(function created(data) {
        assert.isObject(data);
        done();
      })
      .catch(done);
  });

  it('destroyAll()', function assertion(done) {
    const location = chai.create('location');

    tracker.on('query', function mockResult(query, step) {
      [
        function firstQuery() {
          query.response([location]);
        },
        function secondQuery() {
          query.response([{}]);
        },
      ][step - 1]();
    });

    LocationModel.destroyById(location.id)
      .then(function created(data) {
        assert.isObject(data);
        done();
      })
      .catch(done);
  });
});
