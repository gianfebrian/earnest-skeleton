/* global describe, before, after, beforeEach, afterEach, it, chai, assert */

const _ = require('lodash');

const request = require('supertest');

const seeder = require('./../../database/seeders/locations');

const pg = require('./../../build/api/services/pg').default;

const server = require('./../../build/api/server').default;

const LocationModel = require('./../../build/api/models/location').default;

describe('Happy Path', function testCase() {
  before(function setup() {
    return seeder.seed(pg, Promise);
  });

  it.skip('GET /', function assertion() {
    return request(server.app)
      .get('/')
      .expect(200);
  });

  it('GET /locations', function assertion() {
    return request(server.app)
      .get('/locations')
      .expect(200)
      .expect(function serverResponse(res) {
        assert.isArray(res.body);
        if (res.body.length === 0) return;
        assert.isNumber(res.body[0].latitude);
        assert.isNumber(res.body[0].longitude);
      });
  });

  it('GET /location/:id', function assertion() {
    return LocationModel.fetchAll()
      .then(function fetched(collection) {
        const id = collection.first().get('id');
        return request(server.app)
          .get(`/location/${id}`)
          .expect(200)
          .expect(function serverResponse(res) {
            assert.isObject(res.body);
            assert.isNumber(res.body.latitude);
            assert.isNumber(res.body.longitude);
          });
      });
  });

  it('POST /location', function assertion() {
    const location = chai.create('location');
    return request(server.app)
      .post('/location')
      .send(location)
      .expect(200);
  });

  it('PUT /location', function assertion() {
    return LocationModel.fetchAll()
      .then(function fetched(collection) {
        const serialized = collection.serialize();
        const ordered = _.orderBy(serialized, ['label'], ['desc']);
        const transformed = _.transform(ordered, function transform(t, v, k) {
          const newLabel = String.fromCharCode('A'.charCodeAt(0) + k);
          t.push({ id: v.id, label: newLabel });
        }, []);

        return request(server.app)
          .put('/location')
          .send(transformed)
          .expect(200);
      });
  });

  it('PATCH /location', function assertion() {
    return LocationModel.fetchAll()
      .then(function fetched(collection) {
        const location = collection.first().serialize();
        const id = location.id;

        return request(server.app)
          .patch(`/location/${id}`)
          .send({ title: 'Updated' })
          .expect(200);
      });
  });

  it('DELETE /location/:id', function assertion() {
    return LocationModel.forge().orderBy('label', 'ASC').fetchAll()
      .then(function fetched(collection) {
        const location = collection.last();
        const id = location.id;

        return request(server.app)
          .delete(`/location/${id}`)
          .expect(200);
      });
  });

  it('DELETE /locations', function assertion() {
    return request(server.app)
      .delete('/locations')
      .expect(200);
  });
});
