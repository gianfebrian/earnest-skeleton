/* global chai */

const uuid = require('uuid');
const faker = require('faker');

chai.factory('location', {
  id: uuid.v4(),
  title: faker.lorem.words(),
  description: faker.lorem.words(),
  label: faker.lorem.words(),
  latitude: Number(faker.address.longitude()),
  longitude: Number(faker.address.latitude()),
});
