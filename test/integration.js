/* global describe */
/* eslint-disable global-require */

require('dotenv').load();
require('./shared');

describe('Integration Test', function testCase() {
  require('./integration/happy-path.js');
});
