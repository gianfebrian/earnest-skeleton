#!/usr/bin/env bash

mv .env .env.bak 2>/dev/null
touch .env
echo "API_PROTOCOL=$API_PROTOCOL" >> .env
echo "API_HOST=$API_HOST" >> .env
echo "API_PORT=$API_PORT" >> .env
echo "CLIENT_PORT=$CLIENT_PORT" >> .env
echo "MORGAN_FORMAT=$MORGAN_FORMAT" >> .env
echo "LOG_SEVERITY=$LOG_SEVERITY" >> .env
echo "LOG_SILENT=$LOG_SILENT" >> .env
echo "LOG_TRANSPORT=$LOG_TRANSPORT" >> .env
echo "LOG_DIR=$LOG_DIR" >> .env
echo "PAPERTRAIL_HOST=$PAPERTRAIL_HOST" >> .env
echo "PAPERTRAIL_PORT=$PAPERTRAIL_PORT" >> .env
echo "POSTGRE_URL=$POSTGRE_URL" >> .env
echo "POSTGRE_MIN_POOL=$POSTGRE_MIN_POOL" >> .env
echo "POSTGRE_MAX_POOL=$POSTGRE_MAX_POOL" >> .env
echo "REDIS_URL=$REDIS_URL" >> .env
echo "GOOGLE_API_KEY=$GOOGLE_API_KEY" >> .env
