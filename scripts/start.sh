#!/usr/bin/env bash

./scripts/wait-for-it.sh redis:6379 -- ./scripts/wait-for-it.sh postgres:5432 -- npm start
